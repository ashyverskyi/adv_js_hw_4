let serverResponse;

const filmList = document.getElementById('film-list');

fetch("https://ajax.test-danit.com/api/swapi/films")
    .then(response => {
        return response.json();
    })
    .then(data => {
        serverResponse = data;
        console.log(serverResponse);

        const movieNames = serverResponse.map(movie => movie.name);
        console.log(movieNames);

        serverResponse.forEach(movie => {
            const movieItem = document.createElement('li');
            movieItem.innerHTML = `
                        <div class="film-info">
                            <h2>Episode ${movie.episodeId}: ${movie.name}</h2>
                            <p>${movie.openingCrawl}</p>
                        </div>
                    `;
            filmList.appendChild(movieItem);
            fetchCharacters(movie, movieItem);
        });
    })
    .catch(error => {
        console.error("Ошибка:", error);
    });

function fetchCharacters(movie, movieItem) {
    Promise.all(movie.characters.map(url =>
        fetch(url)
            .then(response => response.json())
    ))
        .then(characters => {
            const characterList = characters.map(character => character.name).join(', ');
            const characterElement = document.createElement('p');
            characterElement.textContent = `Персонажи: ${characterList}`;
            movieItem.querySelector('.film-info').appendChild(characterElement);
            console.log(`Персонажи эпизода ${movie.episodeId}: ${movie.name}:`, characters.map(character => character.name));
            console.log("Короткое содержание:", movie.openingCrawl);
        })
        .catch(error => {
            console.error(`Произошла ошибка непреодолимой силы при извлечении данных фильма "${movie.name}":`, error);
        });
}













